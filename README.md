# ansible-vguest

Création Auto d'une VM Vsphere intégrée au dhcp Pfsense et à Ansible
Ajoutez à votre hosts ansible ansible(localhost) vsphere et Pfsense

Change 0 to 1 to enable the installation of mongodb plex dockercompose or rutorrent

## Lancement du playbook via  les roles
#creation de machine
ansible-playbook playbooks/VM_OP.yml -e "VM_NAME=xx VM_IP=xxx.xxx.xxx.xxx OS_CHOICE=CENTOS MDP=motdepasse"

#ajout d'un disque de 50go ici sur la machine 'route'
ansible-playbook playbooks/add_disk_to_VM.yml -e "VM_NAME=route VM_SIZE_Mo=50000000"

#effacer une CM
ansible-playbook playbooks/ERASE_VM.yml -e "VM_NAME=xxx VM_IP=xxx"
