#!/bin/bash
watchdir1=/vol/Ruplex/RUTORRENT/incoming/
watchdir2=/vol/Ruplex/RUTORRENT/incoming/SERIES/
watchdir3=/vol/Ruplex/RUTORRENT/incoming/FILMS/
watchdir4=/vol/Ruplex/RUTORRENT/incoming/LIVRES/


logfile1=/vol/Ruplex/log/watch.all.log
logfile2=/vol/Ruplex/log/watch.series.log
logfile3=/vol/Ruplex/log/watch.films.log
logfile4=/vol/Ruplex/log/watch.livres.log




####1
#while : ; do
#	inotifywait $watchdir1|while read path action file ; do
#                ts=$(date +"%C%y-%m-%d_%H:%M:%S")
#                echo "$ts :: file: $file :: $action :: $path">>$logfile1
#                        if [ "$action" == "CREATE" ] || [ "$action" == "MOVED_FROM" ] || [ "$action" == "CREATE,ISDIR" ]  || [ "$action" == "MOVED_TO" ];  then
#                        echo -e "Subject:inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
#                        fi
#        done
#done &

####2
while : ; do
	inotifywait $watchdir2 -r |while read path action file ; do
                ts=$(date +"%C%y-%m-%d_%H:%M:%S")
                echo "$ts :: file: $file :: $action :: $path">>$logfile2
			    if [ "$action" == "CREATE" ] || [ "$action" == "MOVED_FROM" ] || [ "$action" == "CREATE,ISDIR" ]  || [ "$action" == "MOVED_TO" ];  then
               		echo -e "Subject:inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       			fi
       	        if [ "$action" == "CLOSE_WRITE" ] || [ "$action" == "CLOSE_WRITE,CLOSE" ]; then
       			rsync -r  /vol/Ruplex/RUTORRENT/incoming/SERIES/  /media/freebox/2_SERIES/
       			echo -e "Subject:END inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       			fi
	 done
done &

####3
while : ; do
	inotifywait $watchdir3 -r |while read path action file ; do
                ts=$(date +"%C%y-%m-%d_%H:%M:%S")
                echo "$ts :: file: $file :: $action :: $path">>$logfile3
                if [ "$action" == "CREATE" ] || [ "$action" == "MOVED_FROM" ] || [ "$action" == "CREATE,ISDIR" ]  || [ "$action" == "MOVED_TO" ];  then
                echo -e "Subject:inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       	        fi
       	        if [ "$action" == "CLOSE_WRITE" ] || [ "$action" == "CLOSE_WRITE,CLOSE" ]; then
       			rsync -r  /vol/Ruplex/RUTORRENT/incoming/FILMS/  /media/freebox/1_FILMS/
       			echo -e "Subject:END inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       			fi
        done
done &

####4
while : ; do
	inotifywait $watchdir4 -r |while read path action file ; do
                ts=$(date +"%C%y-%m-%d_%H:%M:%S")
                echo "$ts :: file: $file :: $action :: $path">>$logfile4
                if [ "$action" == "CREATE" ] || [ "$action" == "MOVED_FROM" ] || [ "$action" == "CREATE,ISDIR" ]  || [ "$action" == "MOVED_TO" ];  then
                echo -e "Subject:inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       	        fi
       	        if [ "$action" == "CLOSE_WRITE" ] || [ "$action" == "CLOSE_WRITE,CLOSE" ]; then
       			rsync -r  /vol/Ruplex/RUTORRENT/incoming/LIVRES/  /media/freebox/4_LIVRES/
       			echo -e "Subject:END inotify \n\n $ts :: file: $file :: $action :: $path" | sendmail frederick.loubli@outlook.com
       			fi
        done
done


exit 0
