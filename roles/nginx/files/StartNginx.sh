#!/bin/bash

docker ps -a | awk '{print $1}' | xargs nginx rm -f
cd /appli/docker/nginx && /usr/bin/docker-compose up


exit 0
